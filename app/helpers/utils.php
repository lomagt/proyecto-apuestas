<?php

function vardump($parameter)
{
    var_dump($parameter) and die();
}

function isLoggedIn()
{
    if (isset($_SESSION['user_id'])) {
        return true;
    } else {
        return false;
    }
}

function esActiva($string)
{
    if (strpos($_SERVER['REQUEST_URI'], $string) !== false) {
        return true;
    } else {
        return false;
    }
}

function isAdmin()
{
    if (isset($_SESSION['admin_user'])) {
        if ($_SESSION['admin_user'] === 'SI') {
            return true;
        } else {
            return false;
        }
    }
}
