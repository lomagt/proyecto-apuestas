<?php
/*
     La clase Apuesta permite realizar llamadas a la tabla apuesta en la BBDD, hereda de database de la cual obtiene 
     las herramientas necesarias para hacer las llamadas a la BBDD.
*/
class Apuesta extends Database
{

    private $db;// tiene un solo atributo en el que se instancia un objeto de Database.

    public function __construct()
    {
        $this->db = new Database;
    }

    /*
        getApuestas devuelve un objeto de la clase Apuesta con todas las filas de la tabla apuesta
    */
    public function getApuestas()
    {
        $this->db->query('SELECT * FROM apuesta');

        $apuestas = $this->db->resultSet('Apuesta');

        return $apuestas;
    }

    /*
        addApuestas inserta una apuesta en la tabla apuesta, requiere un array asociativo con los datos a insertar
    */
    public function addApuesta($data)
    {
        $this->db->query("INSERT INTO apuesta (usuario_id, partido_id, pronostico, cantidad) VALUES(:usuario_id, :partido_id, :pronostico, :cantidad)");

        $this->db->bind(':usuario_id', $data['usuario_id']);

        $this->db->bind(':partido_id', $data['id']);

        $this->db->bind(':pronostico', $data['resultado']);

        $this->db->bind(':cantidad', $data['cantidad']);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

    /*
        findUserByIdUserAndIdPartido devuelve una única fila de una tabla (apuesta) donde el usuario haya realizado 
        una apuesta en un partido determinado, requiere de dos parámetros, el id del usuario y el id del partido.
        Devuelve un objeto de la clase Apuesta.
    */
    public function findUserByIdUserAndIdPartido($idPartido,$idUser){

        $this->db->query('SELECT * FROM apuesta WHERE partido_id = :idPartido AND usuario_id = :usuario_id');

        $this->db->bind(':idPartido', $idPartido);

        $this->db->bind(':usuario_id', $idUser);

        $user = $this->db->single('Apuesta');

        return $user;
    }
    
    /*
        delApuesta requiere dos parámetros id usuario y id partido, con esta información realiza un borrado de una 
        fila en la tabla apuesta
    */
    public function delApuesta($id_user, $id_partido){
        
        $this->db->query('DELETE FROM apuesta WHERE usuario_id = :usuario_id AND partido_id = :partido_id');

        $this->db->bind(':usuario_id', $id_user);
       
        $this->db->bind(':partido_id', $id_partido);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

}
