<?php
/*
    La clase Partido hereda de Database desde la cual obtiene las herramientas para conectar con la BBDDD.
    Permite realizar llamadas a la tabla partido de la base de datos
*/
class Partido extends Database
{

    private $db;// tiene un solo atributo en el que se instancia un objeto de Database.

    public function __construct()
    {
        $this->db = new Database;
    }

    /*
        El método addPorra inserta una fila en la tabla partido, requiere de un array asociativo con los datos pertinentes
    */
    public function addPorra($data)
    {
        $this->db->query("INSERT INTO partido (eqlocal, eqvisitante, fecha_hora, image) VALUES(:eqlocal, :eqvisitante, :fecha_hora, :image)");

        $this->db->bind(':eqlocal', $data['equipoLocal']);

        $this->db->bind(':eqvisitante', $data['equipoVisitante']);

        $this->db->bind(':fecha_hora', $data['fecha_hora']);

        $this->db->bind(':image', $data['image']);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

    /*
        El método getPorras devuelve todas las filas de la tabla partido, devuelve un array de objetos de la
        clase Partido.
    */
    public function getPorras()
    {
        $this->db->query('SELECT * FROM partido');

        $partido = $this->db->resultSet('Partido');

        return $partido;
    }

    /*
        El método delPorra requiere un id de una Porra y realiza el borrado de una fila de la tabla partido
    */
    public function delPorra($id)
    {

        $this->db->query('DELETE FROM partido WHERE id = :id');

        $this->db->bind(':id', $id);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

    /*
        El método getPorra requere un id y devuelve una única fila de la tabla partido.
        devuelve un objeto de la clase Partido.
    */
    public function getPorra($id)
    {
        $this->db->query('SELECT * FROM partido WHERE id = :id');

        $this->db->bind(':id', $id);

        $partido = $this->db->single('Partido');

        return $partido;
    }

    /*
        El método updatePorra requiere de un array asociativo, realiza la modificacion de una fila de la tabla partido.
        devuelve true en caso de exito y false en el caso contrario.
    */
    public function updatePorra($data)
    {
        $this->db->query('UPDATE partido SET eqlocal = :eqlocal, eqvisitante = :eqvisitante, image = :image, fecha_hora = :fecha_hora WHERE id = :id');

        $this->db->bind(':id', $data['id_porra']);

        $this->db->bind(':eqlocal', $data['equipoLocal']);

        $this->db->bind(':eqvisitante', $data['equipoVisitante']);

        $this->db->bind(':fecha_hora', $data['fecha_hora']);

        $this->db->bind(':image', $data['image']);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

    /*
        El método updatePorraResultado requiere de un array asociativo, actualiza un registro de una fila de la tabla partido.
        devuelve true en caso de exito y false en el caso contrario.
    */
    public function updatePorraResultado($data)
    {
        $this->db->query('UPDATE partido SET resultado = :resultado WHERE id = :id');

        $this->db->bind(':id', $data['id_porra']);

        $this->db->bind(':resultado', $data['resultado']);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

    /*
        El método cerrarPorra cambia el estado de la porra modificando el registro estado de la tabla partido.
        devuelve true en caso de exito y false en el caso contrario.
    */
    public function cerrarPorra($id, $estado)
    {
        $this->db->query('UPDATE partido SET estado = :estado WHERE id = :id');

        $this->db->bind(':id', $id);

        $this->db->bind(':estado', $estado);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }
}
