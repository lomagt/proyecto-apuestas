<?php
/*
    La clase Usuario permite hacer llamadas a la tabla usuario de la base de datos, hereda de Database todas las herramientas para
    hacer las llamadas a la base de datos en cuestión.
*/ 
class Usuario extends Database
{

    private $db;// tiene un solo atributo en el que se instancia un objeto de Database.

    public function __construct()
    {
        $this->db = new Database;
    }

    /*
        El método getUsers devuelve todos las filas de la tabla usuario, retorna un array de objetos de la clase
        Usuario.
    */
    public function getUsers(){

        $this->db->query('SELECT * FROM usuario');

        $usuarios = $this->db->resultSet('Usuario');

        return $usuarios;
    }

    /*
        El método findUserByEmail devueve una única fila de la tabla usuario,
        retorna un objeto de la clase Usuario.
    */
    public function findUserByEmail($email)
    {

        $this->db->query("SELECT email FROM usuario WHERE email = :email ");

        $this->db->bind(':email', $email);

        $this->db->execute();

        $this->db->single('Usuario');

        if ($this->db->rowCount('Usuario') > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
        El método register insterta una fila en la tabla usuario, requiere un array asociativo con los datos a insertar.
        devuelve true en caso de éxito y false en el caso contraio
    */
    public function register($data)
    {
        $this->db->query("INSERT INTO usuario(nickname, email, password, image) VALUES(:nickname, :email, :password, :image)");

        $this->db->bind(':nickname', $data['nickname']);

        $this->db->bind(':email', $data['email']);

        $this->db->bind(':password', $data['password']);
        
        $this->db->bind(':image', $data['image']);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

    /*
        El método login requiere dos parámetros un email y un password con estos datos verifica que los datos de logeo sean
        correctos.
        Retorna false si la contraseña no es correcta y en caso contrario retorna un objeto con una fila de la tabla usuario.
    */
    public function login($email, $password)
    {
        $this->db->query("SELECT * FROM usuario WHERE email = :email ");

        $this->db->bind(':email', $email);

        $this->db->execute();

        $single = $this->db->single('Usuario');

        if(password_verify($password, $single->password)){
            return $single;
        }else{
            return false;
        }
    }

    /*
        El método getUserById retorna una fila de la tabla usuario con el id que requiere por parametro
    */
    public function getUserById($id){
        
        $this->db->query('SELECT * FROM usuario WHERE id = :id');

        $this->db->bind(':id', $id);

        $user = $this->db->single('Usuario');

        return $user;

    }

    /*
     El método updateUser modifica los datos de una fila de la tabla usuario, requiere un array asociativo con los datos del usuario.
     Retorna true en caso de éxito y false en el caso contrario.
    */
    public function updateUser($data){
        $this->db->query('UPDATE usuario SET nickname = :nickname, email = :email, password = :password, image = :image WHERE id = :id');

        $this->db->bind(':id', $data['id']);

        $this->db->bind(':nickname', $data['nickname']);

        $this->db->bind(':email', $data['email']);
        
        $this->db->bind(':password', $data['password']);
        
        $this->db->bind(':image', $data['image']);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

    /*
        El método findUserByEmailUpdate devuelve una fila de la tabla usuario con los datos de los usuarios que puedan tener 
        el mismo email.
        Retorna true si se encuentra un usuario con el mismo email y false en caso contrario
    */
    public function findUserByEmailUpdate($email, $id)
    {

        $this->db->query("SELECT email FROM usuario WHERE email = :email AND id <> :id");

        $this->db->bind(':email', $email);
        
        $this->db->bind(':id', $id);

        $this->db->execute();

        $this->db->single('Usuario');

        if ($this->db->rowCount('Usuario') > 0) {
            return true;
        } else {
            return false;
        }
    }

}
