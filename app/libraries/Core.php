<?php

class Core
{

    protected $controladorActual = "Paginas"; //Controlador por defecto

    protected $metodoActual = "index"; // Método por defecto

    protected $parametros = []; // Por defecto no hay parámetros

    public function __construct()
    { // mediante este método mágico crearemos un objeto de la clase que lo contiene
        $url = $this->getUrl(); // llama al método getURL() que devuelve un array

        if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) { // Comprueba que existe el fichero en la ruta especificada
            // ucwords convierte en mayúscula la primera letra de una cadena
            $this->controladorActual = ucwords($url[0]); // asigan la cadena convertida a la variable $controladorActual
            unset($url[0]); // destruye la variable especificada
        }
        require_once '../app/controllers/' . $this->controladorActual . '.php'; // llama al controlador obtenido por la variable url
        $this->controladorActual = new $this->controladorActual; //instacia un objeto de la clase del nombre del controlador.

        if (isset($url[1])) { // Comprueba que existe el método en el array url[1] en dicha posición
            if (method_exists($this->controladorActual, $url[1])) { // Comprueba si existe el método en el controlador actual
                $this->metodoActual = $url[1]; // Asigna el nombre del método a la propiedad de la clase metodoActual
                unset($url[1]); // destruye la variable en la posición 1 del array
            }
        }
        // echo $this->metodoActual; // hace un echo de la propiedad metodoActual

        $this->parametros = $url ? array_values($url) : [];// devuelve un array indexado de los valores de un array
        call_user_func_array([$this->controladorActual, $this->metodoActual], $this->parametros);// Llama al controlador y método de la clase del controlador pasandole
        // el parámetro especificado y devuelve el valor que retorne el método de la clase o false en caso de error

    }

    public function getURL()
    {

        if (isset($_GET['url'])) { // Primero se asegura de que el la variable tiene un valor

            $url = rtrim($_GET['url'], '/'); // Retira los espacios en blanco (u otros caracteres) del final de un string

            $url = filter_var($url, FILTER_SANITIZE_URL); // Elimina todos los caracteres extraños menos letras, digitos y los
            // que estan reconocidos por el lenguaje.

            $url = explode('/', $url); // devuelve un array de string separando las palabras por la barra 7

        } else {

            $url[0] = 'Paginas'; // si la variable no tiene un valor al array url se le asigna el valor por defecto de Paginas.

        }

        return $url; // retorna el array url

    }

}
