<?php


class Controller
{
    private $modelActual;

    public function __construct()
    {

    }

    public function model($modelo)
    {
        // Carga el modelo
        if (file_exists('../app/models/' . ucwords($modelo) . '.php')) {
            $this->modelActual = ucwords($modelo);
            require_once '../app/models/' . ucwords($modelo) . '.php';
        }
        //Instancia el modelo y lo devuelve
        $this->modelActual = new $this->modelActual;
        return $this->modelActual;
    }
    public function view($vista, $datos = [])
    {
        // Si el fichero existe lo carga, en caso contrario informa del error y muere
        if (file_exists('../app/views/' . $vista . '.php')) {
            require_once '../app/views/' . $vista . '.php';
        } else {
            die('Error, no existe la vista');
        }
    }

}
