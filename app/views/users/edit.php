
<!-- header page -->
<?php include_once APPROOT.'/views/partials/header.php'; ?>
<!-- Fin header page -->


<div class="container">
    <div class="row">
        <div class="col-0 col-md-2"></div>
        <div class="col-12 col-md-8">
        <div class="card card-body bg-light mt-5">
            <h2> Actualizar datos de perfil</h2>
            <p>Por favor Cambia los datos que quieres actualizar</p>
            <form action="<?= URLROOT ?>/usuarios/edit" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nuevo Nickname: <sup>*</sup></label>
                    <input type="text" name="nickname" class="form-control <?= !empty($datos['nickname_err']) ? 'is-invalid' : '' ?>" value="<?= !empty($datos['nickname']) ? $datos['nickname'] : '' ?>">  
                    <div class="invalid-feedback">
                        <?php echo $datos['nickname_err']  ?>
                    </div>                  
                </div>
                <div class="form-group">
                    <label for="email">Nuevo Email: <sup>*</sup></label>
                    <input type="text" name="email" class="form-control <?= !empty($datos['email_err']) ? 'is-invalid' : '' ?>" value="<?= !empty($datos['email']) ? $datos['email'] : '' ?>">
                    <div class="invalid-feedback">
                       <?= $datos['email_err'] ?>
                    </div>                    
                </div>
                <div class="form-group">
                    <label for="password">Nueva Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?= !empty($datos['password_err']) ? 'is-invalid' : '' ?>" value="<?= !empty($datos['password']) ? $datos['password'] : '' ?>">
                    <div class="invalid-feedback">
                       <?= $datos['password_err'] ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirmar Nueva contraseña: <sup>*</sup></label>
                    <input type="password" name="confirm_password" class="form-control <?= !empty($datos['confirm_password_err']) ? 'is-invalid' : '' ?>" value="<?= !empty($datos['confirm_password']) ? $datos['confirm_password'] : '' ?>"> 
                    <div class="invalid-feedback">
                       <?= $datos['confirm_password_err'] ?>
                    </div>                   
                </div>
                <div class="form-group">
                    <label for="image">Cambiar Foto de perfil: <sup>*</sup></label>
                    <input type="hidden" name="MAX_FILE_SIZE" value="8000000">
                    <input type="file" name="image" class="form-control <?= !empty($datos['image_err']) ? 'is-invalid' : '' ?>" placeholder="Título de la publicación" value="<?= !empty($datos['image']) ? $datos['image'] : '' ?>">
                    <span class="invalid-feedback">
                        <?= $datos['image_err'] ?>
                    </span>
                </div>
                <div class="row mt-4">
                    <div class="col">
                        <input type="submit" value="Actualizar" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
        </div>
        <div class="col-0 col-md-2"></div>
    </div>
</div>

<!-- Fooder page -->
<?php include_once APPROOT.'/views/partials/footer.php'; ?>
<!-- Fooder page -->