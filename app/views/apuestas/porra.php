
<!-- header page -->
<?php include_once APPROOT . '/views/partials/header.php';?>
<!-- Fin header page -->

<!-- NAVBAR -->
<?php include_once APPROOT . '/views/partials/navbar.php';?>
<!-- FIN NAVBAR -->

<div class="container pb-3 pt-3">
    <div class="row">
        <div class="col-0 col-md-2"></div>
        <div class="col-12 col-md-8">
            <div class="container-fluid bg-light rounded-2 p-4">
            <h3>Crea tu porra</h3>
          <form action="<?= URLROOT ?>/apuestas/addPorra" method="post" enctype="multipart/form-data">
          <div class="mb-3">
              <label for="equipoLocalInput" class="form-label"
                >Equipo local:<sup>*</sup></label
              >
              <input
                type="text"
                class="form-control <?= !empty($datos['equipoLocal_err']) ? 'is-invalid' : '' ?>"
                id="equipoLocalInput"
                name="equipoLocal"
                value="<?= !empty($datos['equipoLocal'])? $datos['equipoLocal'] : '' ?>"
              />
              <div class="invalid-feedback">
                <?= $datos['equipoLocal_err'] ?>
              </div>
            </div>
            <div class="mb-3">
              <label for="equipoVisitanteInput" class="form-label"
                >Equipo Visitante:<sup>*</sup></label
              >
              <input
                type="text"
                class="form-control <?= !empty($datos['equipoVisitante_err']) ? 'is-invalid' : '' ?>"
                id="equipoVisitanteInput"
                name="equipoVisitante"
                value="<?= !empty($datos['equipoVisitante'])? $datos['equipoVisitante'] : '' ?>"
              />
              <div class="invalid-feedback">
                <?= $datos['equipoVisitante_err'] ?>
              </div>
            </div>
            <div class="mb-3">
              <label for="fechaPartido" class="form-label"
                >Fecha:<sup>*</sup></label
              >
              <input
                type="date"
                class="form-control <?= !empty($datos['fecha_err']) ? 'is-invalid' : '' ?>"
                id="fechaPartido"
                name="fecha"
                value="<?= !empty($datos['fecha'])? $datos['fecha'] : '' ?>"
              />
              <div class="invalid-feedback">
                <?= $datos['fecha_err'] ?>
              </div>
            </div>
            <div class="mb-3">
              <label for="horaPartido" class="form-label"
                >Hora:<sup>*</sup></label
              >
              <input
                type="time"
                class="form-control <?= !empty($datos['hora_err']) ? 'is-invalid' : '' ?>"
                id="horaPartido"
                name="hora"
                value="<?= !empty($datos['hora'])? $datos['hora'] : '' ?>"
              />
              <div class="invalid-feedback">
                <?= $datos['hora_err'] ?>
              </div>
            </div>
            <div class="mb-3">
                <label for="imagenPartido" class="form-label"
                  >Imagen portada:</label
                >
                <input
                  type="file"
                  class="form-control <?= !empty($datos['image_err']) ? 'is-invalid' : '' ?>"
                  id="imagenPartido"
                  name="image"
                />
                <div class="invalid-feedback">
                  <?= $datos['image_err'] ?>
                </div>
              </div>
            <button type="submit" class="btn btn-primary">Crear</button>
          </form>
          </div>
        </div>
        <div class="col-0 col-md-2"></div>
    </div>
</div>

<!-- Footer page -->
<?php include_once APPROOT . '/views/partials/footer.part.php';?>
<?php include_once APPROOT . '/views/partials/footer.php';?>
<!-- Fin Footer page -->

