<!-- header page -->
<?php include_once APPROOT.'/views/partials/header.php'; ?>
<!-- Fin header page -->

<!-- CONTENT PAGE -->

    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
               
                <h2>Realizar Apuesta</h2>
                <p>Por favor, introduzca un pronóstico y la cantidad que quiere apostar</p>
                
                <form class="row g-3" action="<?= URLROOT ?>/apuestas/setApuesta" method="post">
                    <div class="col-12">
                        <label for="resultadoLocal" class="form-label">Resultado equipo local:</label>
                        <input type="text" class="form-control <?= !empty($datos['eqlocal_err']) ? 'is-invalid' : '' ?>" id="resultadoLocal" name="eqlocal" value="<?= !empty($datos['eqlocal']) ? $datos['eqlocal'] : '' ?>">
                        <div class="invalid-feedback">
                        <?= $datos['eqlocal_err'] ?>
                        </div>                   
                    </div>
                    <div class="col-12">
                        <label for="resultadoVisit" class="form-label">Resultado equipo visitante:</label>
                        <input type="text" class="form-control <?= !empty($datos['eqvisitante_err']) ? 'is-invalid' : '' ?>" id="resultadoVisit" name="eqvisitante" value="<?= !empty($datos['eqvisitante']) ? $datos['eqvisitante'] : '' ?>">
                        <div class="invalid-feedback">
                        <?= $datos['eqvisitante_err'] ?>
                        </div>                   
                    </div>
                    <div class="col-12">
                        <label for="cantidadApuesta" class="form-label">Cantidad de apuesta:</label>
                        <input type="text" class="form-control <?= !empty($datos['cantidad_err']) ? 'is-invalid' : '' ?>" id="cantidadApuesta" name="cantidad" value="<?= !empty($datos['cantidad']) ? $datos['cantidad'] : '' ?>">
                        <div class="invalid-feedback">
                        <?= $datos['cantidad_err'] ?>
                        </div>                   
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary">Crear apuesta</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

<!-- FIN CONTENT PAGE -->

<!-- Fooder page -->
<?php include_once APPROOT.'/views/partials/footer.php'; ?>
<!-- Fooder page -->