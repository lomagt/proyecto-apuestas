
<!-- header page -->
<?php include_once APPROOT . '/views/partials/header.php';?>
<!-- Fin header page -->

<!-- NAVBAR -->
<?php include_once APPROOT . '/views/partials/navbar.php';?>
<!-- FIN NAVBAR -->

<div class="container pb-3 pt-3">
    <div class="row">
        <div class="col-0 col-md-2"></div>
        <div class="col-12 col-md-8">
            <div class="container-fluid bg-light rounded-2 p-4">
            <h3>Introduce un resultado</h3>
          <form action="<?= URLROOT ?>/apuestas/setResultado" method="post">
            <div class="mb-3">
              <label for="equipoLocalInput" class="form-label"
                >Equipo Local:<sup>*</sup></label
              >
              <input
                type="text"
                class="form-control <?= !empty($datos['eqLocal_err']) ? 'is-invalid' : '' ?>"
                id="equipoLocalInput"
                name="eqLocal"
                value="<?= !empty($datos['eqLocal'])? $datos['eqLocal'] : '' ?>"
              />
              <div class="invalid-feedback">
                <?= $datos['eqLocal_err'] ?>
                </div>
            </div>
            <div class="mb-3">
              <label for="equipoVisitanteInput" class="form-label"
                >Equipo Visitante:<sup>*</sup></label
              >
              <input
                type="text"
                class="form-control <?= !empty($datos['eqVisitante_err']) ? 'is-invalid' : '' ?>"
                id="equipoVisitanteInput"
                name="eqVisitante"
                value="<?= !empty($datos['eqVisitante'])? $datos['eqVisitante'] : '' ?>"
              />
              <div class="invalid-feedback">
                <?= $datos['eqVisitante_err'] ?>
                </div>
            </div>
                       
            <input type="hidden" name="id_porra" value="<?= !empty($datos['id_porra'])? $datos['id_porra'] : '' ?>">
            <button type="submit" class="btn btn-primary">Asignar resultado</button>
          </form>
          </div>
        </div>
        <div class="col-0 col-md-2"></div>
    </div>
</div>

<!-- Footer page -->
<?php include_once APPROOT . '/views/partials/footer.part.php';?>
<?php include_once APPROOT . '/views/partials/footer.php';?>
<!-- Fin Footer page -->

