
<!-- header page -->
<?php include_once APPROOT . '/views/partials/header.php';?>
<!-- Fin header page -->

<!-- NAVBAR -->
<?php include_once APPROOT . '/views/partials/navbar.php';?>
<!-- FIN NAVBAR -->

<div class="container pb-3 pt-3">
    <div class="row">
        <div class="col-0 col-md-2"></div>
        <div class="col-12 col-md-8">
            <div class="container-fluid bg-light rounded-2 p-4">
            <h3><?= $datos['partido']->eqlocal ?> VS <?= $datos['partido']->eqvisitante ?></h3>
            <table class="table table-success table-striped">
                <thead>
                    <tr>
                        <th scope="col">Número</th>
                        <th scope="col">Nickname</th>
                        <th scope="col">Pronóstico</th>
                        <th scope="col">Apuesta</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                        $numero = 1;
                        $total = 0;
                        foreach($datos['usuarios'] as $usuario){
                            foreach($datos['apuestas'] as $apuesta){
                                if($usuario->id === $apuesta->usuario_id
                                && $datos['partido']->id === $apuesta->partido_id){
                    ?>
                    <tr>
                        <th scope="row"><?= $numero; ?></th>
                        <td><?= $usuario->nickname ?></td>
                        <td><?= $apuesta->pronostico ?></td>
                        <td><?= $apuesta->cantidad ?> €</td>
                        <?php
                            if(isAdmin()){
                        ?>
                        <td>
                            <a href="<?= URLROOT ?>/apuestas/delParticipante/<?php echo $apuesta->usuario_id.','; echo $apuesta->partido_id ?>" class="btn btn-danger">Eliminar</a>
                        </td>
                        <?php
                            }else{
                        ?>
                        <td>
                            
                        </td>
                        <?php
                            }
                        ?>
                    </tr>
                    <?php
                                    $total = $total + $apuesta->cantidad;
                                    $numero++;
                                }
                            }
                        }
                    ?>

                    <tr>
                        <th scope="col" colspan="3">Ganador/es</th>
                        <th scope="col">Total</th>
                        <th scope="col"><?= $total ?> €</th>
                    </tr>
                    <?php
                        foreach($datos['usuarios'] as $usuario){
                            foreach($datos['apuestas'] as $apuesta){
                                if($usuario->id === $apuesta->usuario_id
                                && $datos['partido']->id === $apuesta->partido_id){
                                    if($apuesta->pronostico === $datos['partido']->resultado){
                    ?>
                    <tr>
                        <td colspan="4"><?= $usuario->nickname ?></td>
                        <td><?= $apuesta->pronostico ?></td>
                    </tr>
                    <?php
                                    }
                                }
                            }
                        }
                    ?>
                </tbody>

            </table>

            </div>
        </div>
        <div class="col-0 col-md-2"></div>
    </div>
</div>

<!-- Footer page -->
<?php include_once APPROOT . '/views/partials/footer.part.php';?>
<?php include_once APPROOT . '/views/partials/footer.php';?>
<!-- Fin Footer page -->

