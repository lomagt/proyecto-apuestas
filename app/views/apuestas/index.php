
<!-- header page -->
<?php include_once APPROOT . '/views/partials/header.php';?>
<!-- Fin header page -->

<!-- NAVBAR -->
<?php include_once APPROOT . '/views/partials/navbar.php';?>
<!-- FIN NAVBAR -->

<div class="container content-center">
    <div class="row">
        <main class="col-12 col-md-8 p-4">
            <div class="flashes">
                <?= (string) flash() ?>
            </div>
            <?php
                foreach($datos['partidos'] as $porra){
            ?>
            <div class="card mb-4">
                <img src="<?= URLROOT ?>/img/<?= $porra->image ?>" class="card-img-top" alt="Imagen partido">
                <div class="card-body">
                    <h5 class="card-title"><?= $porra->eqlocal ?> VS <?= $porra->eqvisitante ?></h5>
                    <p class="card-text">Fecha y hora del partido: <?= $porra->fecha_hora ?></p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Estado: <?= $porra->estado === '1' ? 'Abierta' : 'Cerrada' ?>
                    <?php
                        if(isAdmin()){
                    ?>
                        <?php
                            if($porra->estado === '1'){
                        ?>
                            <div class="d-flex justify-content-end">
                                <a href="<?= URLROOT ?>/apuestas/cambiarEstado/<?= $porra->id ?>" class="btn btn-danger">Cerrar</a>
                            </div>

                        <?php
                            }else{
                        ?>
                            <div class="d-flex justify-content-end">
                                <a href="<?= URLROOT ?>/apuestas/cambiarEstado/<?= $porra->id ?>" class="btn btn-success">Abrir</a>
                            </div>
                        <?php
                            }
                        ?>

                    <?php
                        }
                    ?>
                </li>
                    <li class="list-group-item">Resultado: <?= $porra->resultado === null ? 'Pendiente': $porra->resultado ?>
                    <?php
                        if(isAdmin()){
                    ?>
                    <div class="d-flex justify-content-end">
                        <a href="<?= URLROOT ?>/apuestas/setResultado/<?= $porra->id ?>" class="btn btn-primary">Asignar resultado</a>
                    </div>
                    <?php
                        }
                    ?>
                </li>
                </ul>
                <div class="card-body">
                    <?php
                        if(isAdmin()){
                    ?>
                    <a href="<?= URLROOT ?>/apuestas/updatePorra/<?= $porra->id ?>" class="card-link btn btn-warning">Modificar Porra</a>
                    <a href="<?= URLROOT ?>/apuestas/delPorra/<?= $porra->id ?>" class="card-link btn btn-danger">Eliminar</a>
                    <?php
                        }else{
                    ?>

                    <?php
                        }
                    ?>
                    <a href="<?= URLROOT ?>/apuestas/showApuestas/<?= $porra->id ?>" class="card-link btn btn-success">Ver Participantes</a>
                    <a href="<?= URLROOT ?>/apuestas/traspaso/<?= $porra->id ?>" class="card-link btn btn-primary">Apostar</a>
                </div>
            </div>
            <?php
                }
            ?>
        </main>
        <aside class="col-12 col-md-4">

        </aside>
    </div>
</div>

<!-- Footer page -->
<?php include_once APPROOT . '/views/partials/footer.part.php';?>
<?php include_once APPROOT . '/views/partials/footer.php';?>
<!-- Fin Footer page -->

