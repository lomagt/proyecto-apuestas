<header class="border-4 border-bottom border-success">
      <nav class="navbar navbar-expand-lg navbar-dark bg-success">
        <div class="container-fluid">
          <a class="navbar-brand" href="#"
            ><i class="fas fa-futbol fa-2x"></i
          ></a>
          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link <?=(esActiva('/paginas/index') || esActiva('/apuestas/index') ? 'active' : '');?> fs-5" aria-current="page" href="<?=URLROOT?>/paginas/index">Home</a>
              </li>
              <?php
                if(isAdmin()){
              ?>
              <li class="nav-item">
                <a class="nav-link <?=(esActiva('/apuestas/addPorra') ? 'active' : '');?> fs-5" href="<?=URLROOT?>/apuestas/addPorra">Añadir partido</a>
              </li>
              <?php
                }else{
              ?>

              <?php
                }
              ?>
              <li class="nav-item">
                <a class="nav-link <?=(esActiva('/paginas/about') ? 'active' : '');?> fs-5" href="<?=URLROOT?>/paginas/about">About</a>
              </li>              
            </ul>

            <?php
if (isLoggedIn()) {
    ?>

            <div class="d-flex">
              <ul class="navbar-nav">
                <li class="nav-item dropstart">
                  <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                    <img class="rounded-circle border-white border-3 border" src="<?=URLROOT?>/img/<?=$_SESSION['image_user']?>" alt="Perfil usuario" width="35px">
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <li><a class="dropdown-item" href="<?=URLROOT?>/usuarios/edit">Perfil de ususario</a></li>
                  </ul>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?=URLROOT?>/usuarios/logout"><i class="fas fa-sign-out-alt fa-2x"></i></a>
                </li>
              </ul>
            </div>

            <?php
} else {
    ?>
            <div class="d-flex">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                  <a class="nav-link fs-5" href="<?=URLROOT?>/Usuarios/register"
                    ><i class="far fa-edit"></i> Register</a
                  >
                </li>
                <li class="nav-item">
                  <a class="nav-link fs-5" href="<?=URLROOT?>/usuarios/login"
                    ><i class="fas fa-user"></i> Log in</a
                  >
                </li>
              </ul>
            </div>

            <?php
}
?>

          </div>
        </div>
      </nav>
      <div class="imagen">
        <img src="<?=URLROOT?>/img/istockphoto-1132747055-170667a.jpg" alt="Balon futbol" width="100%">
      </div>
    </header>

