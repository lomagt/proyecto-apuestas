<footer class="container-fluid">
      <div class="row mt-5 mb-5">
        <div class="social-media col-12 col-md-3">
          <a href="#"><i class="fab fa-instagram"></i></a>
        </div>
        <div class="social-media col-12 col-md-3">
          <a href="#"><i class="fab fa-facebook-square"></i></a>
        </div>
        <div class="social-media col-12 col-md-3">
          <a href="#"><i class="fab fa-twitter"></i></a>
        </div>
        <div class="social-media col-12 col-md-3">
          <a href="#"><i class="fab fa-behance"></i></a>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-12 col-md-4">
          <div class="section-footer d-flex justify-content-around">
            <a href="#">Work</a> 
            <a href="#">Story</a>
            <a href="#">Services</a>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <div class="section-footer d-flex justify-content-around">
            <p>Creado por Manuel Mañas Alfaro</p>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <div class="section-footer d-flex justify-content-around">
            <a href="#">Careers</a>
            <a href="#">contact us</a>
          </div>
        </div>
      </div>
    </footer>