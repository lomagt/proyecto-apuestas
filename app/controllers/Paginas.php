<?php

use \Tamtamchik\SimpleFlash\Flash;

class Paginas extends Controller
{

    public function __construct()
    {
        if (isLoggedIn()) {
            redirect('/apuestas/index');
        }
        // Desde aquí cargaremos los modelos.
    }

    /*
    El método index carga la vista principal de la aplicación
    */
    public function index(){
        $data = [
            "Titulo" => "Framework de Manuel Mañas Alfaro"
        ];
        $this->view('paginas/index',$data);
    }

}
