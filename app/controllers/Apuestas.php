<?php

use \Tamtamchik\SimpleFlash\Flash;

// Permite usar una libreria para mensajes flash
/*
    La clase apuestas realiza todo lo que tenga que ver con las apuestas,
    hereda de Controller y con esta los métodos de esta clase realizamos todas
    las interaciones con la porra
*/
class Apuestas extends Controller
{
    /*
        La clase tiene tres propiedades con las que ser realizaran la llamadas a los modelos posteriormente
    */
    private $modelUsuario;
    private $modelApuesta;
    private $modelPartido;
    /*
        En el contructor se realizan las instancias de los modelos y se comprueba que el ususario este logeado
    */
    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('/users/login');
        }
        $this->modelPartido = $this->model('Partido');
        $this->modelApuesta = $this->model('Apuesta');
        $this->modelUsuario = $this->model('Usuario');
    }

    /*
    El método index carga la vista principal de la aplicación despues de loguearse
    */
    public function index()
    {
        $partidos = $this->modelPartido->getPorras();
        $data = [
            "partidos" => $partidos,
        ];
        $this->view('apuestas/index', $data);

    }
    /*
        Desde addPorra crearemos una porra nueva, el método llama al modelo partido desde el cual
        se introducen en la base de datos sus registros, tambien llama a la clase file con la cual 
        podemos introducir una imagen en la aplicación la cual es llamada mediante una url o nombre
        de la imagen desde la BBDD
    */
    public function addPorra()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'equipoLocal' => trim($_POST['equipoLocal']),
                'equipoVisitante' => trim($_POST['equipoVisitante']),
                'fecha' => trim($_POST['fecha']),
                'hora' => trim($_POST['hora']),
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'equipoLocal_err' => '',
                'equipoVisitante_err' => '',
                'fecha_err' => '',
                'hora_err' => '',
                'image_err' => '',
                'fecha_hora' => '',
            );

            if (empty($data['equipoLocal'])) {
                $data['equipoLocal_err'] = 'Please choose a team.';
            }

            if (empty($data['equipoVisitante'])) {
                $data['equipoVisitante_err'] = 'Please choose a team.';
            }

            if (empty($data['fecha'])) {
                $data['fecha_err'] = 'Please choose a date.';
            }

            if (empty($data['hora'])) {
                $data['hora_err'] = 'Please choose a time.';
            }

            if (!empty($data['image'])) {
                $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                $newFile = new File($_FILES['image'], $arrTypes);
                try {

                    //Complétalo
                    $newFile->errorFile();
                    $newFile->saveUploadFile('img/');

                } catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                }
            }

            if (empty($data['equipoLocal_err']) && empty($data['equipoVisitante_err']) && empty($data['fecha_err']) && empty($data['hora_err']) && empty($data['image_err'])) {

                $data['fecha_hora'] = $data['fecha'] . ' ' . $data['hora'];

                $this->modelPartido->addPorra($data);

                $flash = new Flash();
                $flash->message('Añadia porra con éxito', 'info');

                redirect('/apuestas/index');
            } else {
                $this->view('apuestas/porra', $data);
            }

        } else {
            $data = array(
                'equipoLocal' => '',
                'equipoVisitante' => '',
                'fecha' => '',
                'hora' => '',
                'image' => '',
                'equipoLocal_err' => '',
                'equipoVisitante_err' => '',
                'fecha_err' => '',
                'hora_err' => '',
                'image_err' => '',
            );
            $this->view('apuestas/porra', $data);
        }

    }

    /*
        El metodo dePorra borra una porra requiere un id que le llega por método GET, llama al modelo 
        partido desde el cual se hace la llamada a la base de datos y se realiza el borrado
    */
    public function delPorra($id)
    {
        $borrar = $this->modelPartido->delPorra($id);
        if ($borrar) {
            $flash = new Flash();
            $flash->message('Porra borrada con exito', 'info');

            redirect('/apuestas/index');
        } else {
            $flash = new Flash();
            $flash->message('No se ha podido borrar la porra', 'info');

            redirect('/apuestas/index');
        }
    }

    /*
        Desde el método showApuestas que requiere el id de la porra veremos los participantes en la porra, llama 
        a los tres modelos de la base de datos
    */
    public function showApuestas($id)
    {
        $partido = $this->modelPartido->getPorra($id);
        $apuestas = $this->modelApuesta->getApuestas();
        $usuarios = $this->modelUsuario->getUsers();

        $data = [
            'partido' => $partido,
            'apuestas' => $apuestas,
            'usuarios' => $usuarios,
        ];

        $this->view('apuestas/participantes', $data);
    }

    /*
        El método traspaso requiere un id el cual es pasado a una variable global sesión que se usara en el método
        setApuesta con el objetivo de que el usuario no pueda cambiar el id de la porra en la que esta apostando,
        tambien comprueba que el usuario no tenga ya una apuesta en la porra de forma que si ya ha realizado una apuesta
        lo redirige a la vista principal y genera un mensaje flash donde le dice que no puede apostar otra vez,
        ademas comprueba que la porra no este cerrada, en el caso de que este cerrada lo redirige a la vista principal
        y genera un mensaje Flash con la información.
        LLama a los modelos Apuesta y Partido.
    */
    public function traspaso($id)
    {

        if ($this->modelApuesta->findUserByIdUserAndIdPartido($id, $_SESSION['user_id'])) {
            $flash = new Flash();
            $flash->message('No puedes volver a apostar', 'info');
            redirect('/apuestas/index');
        } else {

            $porraCerrada = $this->modelPartido->getPorra($id);
            
            if($porraCerrada->estado === '0'){

                $flash = new Flash();
                $flash->message('No se puede Modificar, porra cerrada', 'warning');
                redirect('/apuestas/index');

            }else{

                $_SESSION['id'] = $id;
    
                redirect('/apuestas/setApuesta');

            }
        }

    }

    /*
        Desde el método setApuesta se realizan la apuestas en la porras, llama al modelo Apuesta
        y trabaja con las vista apostar
    */
    public function setApuesta()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Combrueba si la variable global $_SERVER recibe una post y hará algo si es true

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'id' => $_SESSION['id'],
                'usuario_id' => trim($_SESSION['user_id']),
                'eqlocal' => trim($_POST['eqlocal']),
                'eqvisitante' => trim($_POST['eqvisitante']),
                'cantidad' => trim($_POST['cantidad']),
                'eqlocal_err' => '',
                'eqvisitante_err' => '',
                'cantidad_err' => '',
                'resultado' => '',
            );

            if (empty($data['eqlocal']) && $data['eqlocal'] !== '0') {
                $data['eqlocal_err'] = 'Please choose a number.';
            } else {
                if (!is_numeric($data['eqlocal'])) {
                    $data['eqlocal_err'] = 'Please choose a number.';
                }
            }

            if (empty($data['eqvisitante']) && $data['eqvisitante'] !== '0') {
                $data['eqvisitante_err'] = 'Please choose a number.';
            } else {
                if (!is_numeric($data['eqvisitante'])) {
                    $data['eqvisitante_err'] = 'Please choose a number.';
                }
            }

            if (empty($data['cantidad']) && $data['cantidad'] === '0') {
                $data['cantidad_err'] = 'choose a number greater than 0.';
            } else if (!is_numeric($data['cantidad'])) {
                $data['cantidad_err'] = 'Please choose a number.';
            }

            if (empty($data['cantidad_err']) && empty($data['eqvisitante_err']) && empty($data['eqlocal_err'])) {

                $data['resultado'] = $data['eqlocal'] . '-' . $data['eqvisitante'];
                $this->modelApuesta->addApuesta($data);

                $flash = new Flash();
                $flash->message('Apuesta realizada con éxito', 'info');
                unset($_SESSION['id']);
                redirect('/apuestas/index');

            } else {
                $this->view('apuestas/apostar', $data);
            }

        } else { // Si no crea un array data asociativo con sus claves vacias
            $data = [
                'id' => '',
                'usuario_id' => '',
                'eqlocal' => '',
                'eqvisitante' => '',
                'cantidad' => '',
                'eqlocal_err' => '',
                'eqvisitante_err' => '',
                'cantidad_err' => '',
            ];
            $this->view('apuestas/apostar', $data); // Y ejecuta el método view de la clase padre pasandole el array como parámetro
        }
    }

    /*
        Desde el método delParticipante el admin puede borrar una apuesta en el caso de que no haya sido pagada o el usuario quiera 
        cambiar la apuesta, el método recibe un parametro del cual se separa con la función explode la cual genera un array
        de hay que se le llame array al parametro aunque no lo sea.
        El método llama a los modelo Apuesta.
    */
    public function delParticipante($array)
    {
        $campos = explode(",", $array);
        $borrar = $this->modelApuesta->delApuesta($campos[0], $campos[1]);
        if ($borrar) {
            $flash = new Flash();
            $flash->message('Apuesta borrada con exito', 'info');

            redirect("/apuestas/showApuestas/", $campos[1]);
        } else {
            $flash = new Flash();
            $flash->message('No se ha podido borrar la apuesta', 'info');

            redirect('/apuestas/index');
        }
    }

    /*
        Desde el método updatePorra modificamos los datos de una porra por si se creó con algun error,
        llama al modelo Partido.
    */
    public function updatePorra($id = '')
    {
        if (!empty($id)) {
            $porra = $this->modelPartido->getPorra($id);
            $fecha = date("Y-m-d", strtotime($porra->fecha_hora));
            $hora = str_replace('-', ':', date("H-i", strtotime($porra->fecha_hora)));
            
            if($porra->estado === '0'){

                $flash = new Flash();
                $flash->message('No se puede Modificar, porra cerrada', 'warning');
                redirect('/apuestas/index');  
            }

            $datos = [
                'id_porra' => $id,
                'equipoLocal' => $porra->eqlocal,
                'equipoVisitante' => $porra->eqvisitante,
                'fecha' => $fecha,
                'hora' => $hora,
                'image' => '',
                'equipoLocal_err' => '',
                'equipoVisitante_err' => '',
                'fecha_err' => '',
                'hora_err' => '',
                'image_err' => '',
            ];
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'id_porra' => trim($_POST['id_porra']),
                'equipoLocal' => trim($_POST['equipoLocal']),
                'equipoVisitante' => trim($_POST['equipoVisitante']),
                'fecha' => trim($_POST['fecha']),
                'hora' => trim($_POST['hora']),
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'fecha_hora' => '',
                'equipoLocal_err' => '',
                'equipoVisitante_err' => '',
                'fecha_err' => '',
                'hora_err' => '',
                'image_err' => '',
                'fecha_hora' => '',
            );

            if (empty($data['equipoLocal'])) {
                $data['equipoLocal_err'] = 'Please choose a team.';
            }

            if (empty($data['equipoVisitante'])) {
                $data['equipoVisitante_err'] = 'Please choose a team.';
            }

            if (empty($data['fecha'])) {
                $data['fecha_err'] = 'Please choose a date.';
            }

            if (empty($data['hora'])) {
                $data['hora_err'] = 'Please choose a time.';
            }

            if (!empty($data['image'])) {
                $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                $newFile = new File($_FILES['image'], $arrTypes);
                try {

                    //Complétalo
                    $newFile->errorFile();
                    $newFile->saveUploadFile('img/');

                } catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                }
            }

            if (empty($data['equipoLocal_err']) && empty($data['equipoVisitante_err']) && empty($data['fecha_err']) && empty($data['hora_err']) && empty($data['image_err'])) {

                $data['fecha_hora'] = $data['fecha'] . ' ' . $data['hora'];

                if (empty($data['image'])) {
                    $porra1 = $this->modelPartido->getPorra($data['id_porra']);
                    $data['image'] = $porra1->image;
                    $bool = $this->modelPartido->updatePorra($data);
                } else {
                    $this->modelPartido->updatePorra($data);
                }

                $flash = new Flash();
                $flash->message('Modificada porra con éxito', 'info');

                redirect('/apuestas/index');
            } else {
                $this->view('apuestas/updatePorra', $data);
            }

        } else {
            $data = array(
                'equipoLocal' => '',
                'equipoVisitante' => '',
                'fecha' => '',
                'hora' => '',
                'image' => '',
                'equipoLocal_err' => '',
                'equipoVisitante_err' => '',
                'fecha_err' => '',
                'hora_err' => '',
                'image_err' => '',
            );
            $this->view('apuestas/updatePorra', $datos);
        }

    }

    /*
        Desde el método setResultado introducimos un resultado final a la porra,
        llama al modelo Partido
    */
    public function setResultado($id = '')
    {

        if (!empty($id)) {

            $porraCerrada = $this->modelPartido->getPorra($id);
            
            if($porraCerrada->estado === '0'){

                $flash = new Flash();
                $flash->message('No se puede Modificar, porra cerrada', 'warning');
                redirect('/apuestas/index');  
            }

            $datos = [
                'id_porra' => $id,
            ];
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'id_porra' => trim($_POST['id_porra']),
                'eqLocal' => trim($_POST['eqLocal']),
                'eqVisitante' => trim($_POST['eqVisitante']),
                'resultado' => '',
                'eqLocal_err' => '',
                'eqVisitante_err' => '',
            );

            if (empty($data['eqLocal']) && $data['eqLocal'] !== '0') {
                $data['eqLocal_err'] = 'Please choose a result.';
            } else {
                if (!is_numeric($data['eqLocal'])) {
                    $data['eqLocal_err'] = 'Please choose a number.';
                }
            }

            if (empty($data['eqVisitante']) && $data['eqVisitante'] !== '0') {
                $data['eqVisitante_err'] = 'Please choose a result.';
            } else {
                if (!is_numeric($data['eqVisitante'])) {
                    $data['eqVisitante_err'] = 'Please choose a number.';
                }
            }

            if (empty($data['eqLocal_err']) && empty($data['eqVisitante_err'])) {

                $data['resultado'] = $data['eqLocal'] . '-' . $data['eqVisitante'];
                $this->modelPartido->updatePorraResultado($data);

                $flash = new Flash();
                $flash->message('Resultado introducido con éxito', 'info');

                redirect('/apuestas/index');
            } else {
                $this->view('apuestas/setResultado', $data);
            }

        } else {

            $this->view('apuestas/setResultado', $datos);
        }

    }

    /*
        Desde el método cambiarEstado cerramos la porra o la abrimos, si se cierra ya no se podran realizar cambios en 
        la porra.
        Llama al modelo Partido.
    */
    public function cambiarEstado($id)
    {
        $porra = $this->modelPartido->getPorra($id);
        if ($porra->estado === '1') {
            $this->modelPartido->cerrarPorra($id, 0);
            redirect('/apuestas/index');
        } else {
            $this->modelPartido->cerrarPorra($id, 1);
            redirect('/apuestas/index');
        }
    }

}
