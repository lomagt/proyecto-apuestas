<?php

use \Tamtamchik\SimpleFlash\Flash;

// Permite usar una libreria para mensajes flash
/*
    Desde la clase Usuarios que hereda de Controller controlamos todo lo que tiene que ver con los usuarios
    tiene una propiedad y con la cual llamamos el modelo Usuario
*/
class Usuarios extends Controller
{

    private $userModel;

    public function __construct()
    {
        $this->userModel = $this->model('Usuario');
    }
    /*
     El método register ingresa los datos de un usuario en la base de datos, llama al modelo usuario
    */
    public function register()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Combrueba si la variable global $_SERVER recibe una post y hará algo si es true

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'nickname' => trim($_POST['nickname']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirm_password' => trim($_POST['confirm_password']),
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'nickname_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => '',
                'image_err' => '',
            );

            if (empty($data['nickname'])) {
                $data['nickname_err'] = 'Please choose a username.';
            }

            if (empty($data['email'])) {
                $data['email_err'] = 'Please choose a email.';
            } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email_err'] = 'Please choose a valid email.';
            } else if ($this->userModel->findUserByEmail($data['email'])) {
                $data['email_err'] = 'Please choose an unrepeated email.';
            }

            if (empty($data['password'])) {
                $data['password_err'] = 'Please provide a valid password.';
            } else if (isset($data['password'])) {
                if (strlen($data['password']) < 6) {
                    $data['password_err'] = 'Please provide a valid password with 6 or more digits.';
                }
            }

            if (empty($data['confirm_password'])) {
                $data['confirm_password_err'] = 'Please repeat a valid password.';
            } else {
                if ($data['confirm_password'] != $data['password']) {
                    $data['confirm_password_err'] = 'Please repeat a valid password.';
                }
            }

            if (!empty($data['image'])) {
                $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                $newFile = new File($_FILES['image'], $arrTypes);
                try {

                    //Complétalo
                    $newFile->errorFile();
                    $newFile->saveUploadFile('img/');

                } catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                }
            }

            if (empty($data['nickname_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['confirm_password_err']) && empty($data['image_err'])) {

                //echo "to-do:registrar usuario";

                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                $this->userModel->register($data);

                $flash = new Flash();
                $flash->message('Ya estás registrado y puedes iniciar sesión.', 'info');

                redirect('/usuarios/login');
            } else {
                $this->view('users/register', $data);
            }

        } else { // Si no crea un array data asociativo con sus claves vacias
            $data = [
                'nickname' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'image' => '',
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => '',
                'image_err' => '',
            ];
            $this->view('users/register', $data); // Y ejecuta el método view de la clase padre pasandole el array como parámetro
        }
    }
    /*
        El método login permite logear un usuario ya registrado, para ello comprueba que el usuario este registrado
        y una vez que todo es correcto redirige a la vista principal de la aplicaión donde podrá ver las apuestas 
        permitidas.
        llama al modelo Usuario.
    */
    public function login()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Combrueba si la variable global $_SERVER recibe una post y hará algo si es true

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => '',
            );

            if (empty($data['email'])) {
                $data['email_err'] = 'Please choose a email.';
            } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email_err'] = 'Please choose a valid email.';
            }

            if (empty($data['password'])) {
                $data['password_err'] = 'Please provide a valid password.';
            } else if (isset($data['password'])) {
                if (strlen($data['password']) < 6) {
                    $data['password_err'] = 'Please provide a valid password with 6 or more digits.';
                }
            }

            if (empty($data['email_err']) && empty($data['password_err'])) {

                if ($this->userModel->findUserByEmail($data['email'])) {

                    $user = $this->userModel->login($data['email'], $data['password']);
                    if ($user) {
                        // Lo haremos más adelante: creamos variables de sesión
                        $this->createUserSession($user);
                    } else {
                        $data['password_err'] = 'Contraseña incorrecta';
                        $this->view('users/login', $data);
                    }

                } else {
                    $data['email_err'] = 'El usuario no se ha encontrado';
                }

            } else {
                $this->view('users/login', $data);
            }

        } else { // Si no crea un array data asociativo con sus claves vacias
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => '',
            ];
            $this->view('users/login', $data); // Y ejecuta el método view de la clase padre pasandole el array como parámetro
        }
    }

    /*
     Desde el método edit se podrá editar un usuario registrado, solo permite hacerlo al usuario en cuestión.
     llama al modelo Usuario
    */
    public function edit()
    {

        $usuarioData = $this->userModel->getUserById($_SESSION['user_id']);

        $datos = array(
            'id' => $usuarioData->id,
            'nickname' => $usuarioData->nickname,
            'email' => $usuarioData->email,
            'password' => $usuarioData->password,
            'confirm_password' => $usuarioData->password,
            'image' => $usuarioData->image,
            'nickname_err' => '',
            'email_err' => '',
            'password_err' => '',
            'confirm_password_err' => '',
            'image_err' => '',
        );

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'id' => $datos['id'],
                'nickname' => trim($_POST['nickname']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirm_password' => trim($_POST['confirm_password']),
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'nickname_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => '',
                'image_err' => '',
            );

            if (empty($data['nickname'])) {
                $data['nickname_err'] = 'Please choose a username.';
            }

            if (empty($data['email'])) {
                $data['email_err'] = 'Please choose a email.';
            } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email_err'] = 'Please choose a valid email.';
            } else if ($this->userModel->findUserByEmailUpdate($data['email'], $datos['id'])) {
                $data['email_err'] = 'Please choose an unrepeated email.';
            }

            if (empty($data['password'])) {
                $data['password_err'] = 'Please provide a valid password.';
            } else if (isset($data['password'])) {
                if (strlen($data['password']) < 6) {
                    $data['password_err'] = 'Please provide a valid password with 6 or more digits.';
                }
            }

            if (empty($data['confirm_password'])) {
                $data['confirm_password_err'] = 'Please repeat a valid password.';
            } else {
                if ($data['confirm_password'] != $data['password']) {
                    $data['confirm_password_err'] = 'Please repeat a valid password.';
                }
            }

            if (!empty($data['image'])) {
                $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                $newFile = new File($_FILES['image'], $arrTypes);
                try {

                    //Complétalo
                    $newFile->errorFile();
                    $newFile->saveUploadFile('img/');

                } catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                }
            }

            if (empty($data['nickname_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['confirm_password_err']) && empty($data['image_err'])) {

                //echo "to-do:registrar usuario";

                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                if(empty($data['image'])){
                    
                    $data['image'] = $usuarioData->image;
                    $this->userModel->updateUser($data);
                    
                }else{
                    $this->userModel->updateUser($data);
                }
                

                $flash = new Flash();
                $flash->message('Cambios de perfil confirmados', 'info');

                redirect('/apuestas/index');
            } else {
                $this->view('users/edit', $data);
            }

        } else { // Si no crea un array data asociativo con sus claves vacias
            
            $this->view('users/edit', $datos); // Y ejecuta el método view de la clase padre pasandole el array como parámetro
        }

    }
    /*
     El método createUserSession requere un objeto de usuario mediante una llamada a la tabla usuario de la base de datos, 
     aqui se crea la sesión de usuario y sus variables globales.
    */
    public function createUserSession($user)
    {

        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_name'] = $user->nickname;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['image_user'] = $user->image;
        $_SESSION['admin_user'] = $user->admin;

        redirect('/apuestas/index');
    }

    /*
     El método logout cierra la sesión de usuario
    */
    public function logout()
    {

        unset($_SESSION['user_id']);
        unset($_SESSION['user_name']);
        unset($_SESSION['user_email']);
        unset($_SESSION['image_user']);
        unset($_SESSION['admin_user']);
        session_destroy();

        redirect('/paginas/index');
    }

}
